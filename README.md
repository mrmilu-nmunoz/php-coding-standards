# Mr.Milu PHP Team Coding Standards

Welcome to the Mr.Milu PHP Coding Standards repository! This repo provides 
guidelines for writing consistent and efficient PHP code within the organization. 
Adhering to these standards ensures high code quality and ease of collaboration. 
A valuable resource for developers of all levels.

## Accepted Coding Standards

These coding standards have been formally accepted and are to be adhered to 
within the organization.

## [PSR-12](https://www.php-fig.org/psr/psr-12/)

The PSR-12 Coding Style Guide provides guidelines for writing clear, consistent,
and maintainable PHP code. Developed by the PHP Framework Interop Group, these
standards promote best practices and make collaboration easier. Following PSR-12
ensures efficient, readable code for developers of all levels.

### Project types where using PSR-12 Coding Style Guide is mandatory.

- Any project base in PHP Framework like Symfony or Laravel.

## [Drupal Coding Standards](https://www.drupal.org/docs/develop/standards/php/php-coding-standards)

The Drupal Coding Standards provide guidelines for writing clean, efficient PHP
code within the Drupal framework. Adherence ensures consistency, maintainability,
and reusability for all developers. An essential resource for writing better code.


This document is loosely based on the [PEAR Coding standards](https://pear.php.net/manual/en/standards.php).

### Why using a different coding standard for Drupal

On February 2023, the team made a formal endorsement of the current Drupal Coding 
Standard to uphold consistency and uniformity within the Drupal codebase.

This could change if Drupal switches to using PSR-12 in the future.

### Usage

This coding standard is only meant to be used for Drupal projects.

## Tools

Here is a comprehensive list of tools using to enforce, lint and apply these standards:
