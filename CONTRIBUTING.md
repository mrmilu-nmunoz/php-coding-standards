# Contribution guidelines

Members of the PHP Team are encouraged and welcome to make contributions to this repository.

## How to contribute

- Create a branch with a descriptive and concise name that outlines the purpose of the proposed modification or addition.
- Make the necessary changes or additions to the code.
- Submit a Merge Request (MR) and include all members of the team as reviewers. If the MR is not yet complete, mark it as a draft to allow for contributions and corrections from other members.
- When it is ready, request a review of the MR in the #php slack channel.
- Depending on the nature and content of the MR, it may be discussed during the next PHP Team Weekly meeting.

## Approval proccess

In order to merge a Merge Request (MR) into the main branch, a majority of votes must be obtained within the MR.


## Requirements

- The content must be written in English. If necessary, it is recommended to use a translation tool such as deepl.com.
- The wording used in the content must be formal.
- If the content contains acronyms, they must be written in full on the first occurrence, followed by the acronym in parentheses (i.e., Merge Request (MR)).